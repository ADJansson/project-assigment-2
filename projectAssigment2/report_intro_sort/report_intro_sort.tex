% What kind of text document should we build
\documentclass[a4,10pt]{article}


% Include packages we need for different features (the less, the better)

% Clever cross-referencing
\usepackage{cleveref}

% Math
\usepackage{amsmath}

% Algorithms
\usepackage{algorithm}
\usepackage{algpseudocode}

% Tikz
\RequirePackage{tikz}
\usetikzlibrary{arrows,shapes,calc,through,intersections,decorations.markings,positioning}

\tikzstyle{every picture}+=[remember picture]

\RequirePackage{pgfplots}


% Set TITLE, AUTHOR and DATE
\title{A benchmark of the introspective sorting algorithm using insertion sort and STL's heap sort}
\author{Andreas Dyr{\o}y Jansson}
\date{\today}
 


\begin{document}



  % Create the main title section
  \maketitle

  \begin{abstract}
    This article describes the process of implementing an introspective sorting algorithm using insertion sort and heap sort.
    The results are compared to the standard STL sorting algorithm.
  \end{abstract}


  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %%  The main content of the report  %%
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
  \section{Introduction}
The C++ programming language contains a library named the Standard Template Library, which contains a sorting algorithm
implemented using introspective sorting. This report discusses the results of creating a custom
implementation using insertion sort and heap sort, and comparing the benchmarks of different sorting algorithms.

  \section{Introspective Sort}
  The Introspective Sorting Algorithm works by utilizing two different kinds of sorting algorithms on a vector (see \cref{fig:algorithm1}). The vector is
  first partially sorted using one algorithm, and then sent to a new algorithm after a certain number of iterations. Due to this,
  the first algorithm should be efficient on random vectors, while the second should be faster on partially sorted data. This
  way the best sorting algorithm is applied to the vector, speeding up sorting time.
  The introspective sorting algorithm normally uses heap sort and insertion sort depending on the arrangement of data, along
  with some clever partitioning. According to sorting-algorithms.com, insertion sort is the best choice for partially
  sorted data\cite{insalgp:2015}. Heap sort works by putting all elements into a binary tree, and then removing the
  largest elements in succession to create a sorted list (Chapter 25.5 - Heap Sort\cite{liang:2013}).
  A custom implementation of insertion sort along with STL's heap sort was used to implement the introspective sorting algorithm
  discussed in this paper, as can be seen in \cref{fig:algorithm2}. Partitioning was done using STL's built-in partitioning
  function. Due to this implementation, the algorithm is expected to perform worse than STL's implementation.
  The algorithm for introspective sorting was taken from a paper on Introspective sorting by Musser\cite{musser:2015}.
 \begin{algorithm}
      \caption{Introspective algorithm psuedocode}\label{introspec}
      \begin{algorithmic}[1]
        \Procedure{Introsort}{f, b}\Comment{Inputs: f: Iterator pointing to the first element, b: Iterator pointing to the last element}
        \State
        \textbf{introsortLoop}(f, b, 2  floor(log(f $-$ b))
        \State
        \textbf{insertion\_sort} (f, b)
        \EndProcedure
      \end{algorithmic}
      \label{fig:algorithm1}
    \end{algorithm}

 \begin{algorithm}
      \caption{Introsort loop psuedocode}
      \begin{algorithmic}[2]
        \Procedure{Introsort loop}{f, b, depthLimit}\Comment{depthLimit - number of times heap\_sort should be run}
        \While{b $-$ f $>$ sizeThreshold}
        \If{depthLimit $==$ 0} \State \textbf{heap\_sort}(f, b)
	\Return
	\EndIf
        \State depthLimit $=$ depthLimit $-$ 1;
        \State p $=$ Partition(f, b)
        \State \textbf{introsortLoop}(p, b, depthLimit)
        \State b $=$ p;
        \EndWhile\label{euclidendwhile}
        \EndProcedure
      \end{algorithmic}
      \label{fig:algorithm2}
    \end{algorithm}

  \section{Benchmark set-up}
 The benchmark was set up by creating a new .cpp-file containing code to run the benchmarks. This file contains functions to
 generate random data, keep track of execution time, and create .dat-files containing the results. The .dat-files were
 imported in LayTex to display the results  in \cref{fig:bench_sort} and \cref{fig:intro_vs_stl}. The intro sort algorithm was
 tested with a randomly generated
 dataset ranging from 20 to 100,000 integers. In order to put the results into context, other sorting algorithms were
 benchmarked simultaneously with the same dataset. The algorithms used were shell sort, bubble sort,
 insertion sort, STL's heap sort and the standard STL sorting algorithm. The execution time of the sorting operations were
 measured using the high precision
 chrono device included with the Standard Template Library in C++. Benchmarking all the algorithms on the same hardware with
 the same dataset creates a relative basis for comparing the execution times.

    \begin{itemize}
      \item All benchmarks were performed on a computer with the following specifications:
	\subitem Intel i5-4210U@2.40GHz
        \subitem 6.00 GB RAM
	\subitem Windows 10 x64
      \item For the sorting algorithms, a dataset of $N$ integers were sorted, where duration times were measured by using
      the high precision chrono device which is included with the STL.
    \end{itemize}

  \section{Results}
As expected, the implementation using custom insertion sort and heap sort performed worse than the standard STL sorting
algorithm. However, the implementation did perform close to STL. This may be
hard to see from the overlapping black and green lines in \cref{fig:bench_sort}, but it is clear in \cref{fig:intro_vs_stl},
where only the times of the introsort implementation and STL's heap sort and sort are plotted. The fact that heap sort performed
worse than introsort backs up the claim that heap sort is best for random datasets, while insertion sort is best for nearly
sorted data.
    \begin{figure}[btp]
      \begin{tikzpicture}
        \begin{axis}[
          xmajorgrids=false,ymajorgrids=false,
          legend pos=north west,
          width=1.0\textwidth, height=0.25\textheight,
            %reverse legend
          ]

          \addplot[green] table[x=size ,y=time, skip first n=0] {dat/benchmark_stl_sort.dat};
          \addplot[red] table[x=size ,y=time, skip first n=0] {dat/benchmark_mylib_bubble.dat};
          \addplot[blue] table[x=size ,y=time, skip first n=0] {dat/benchmark_mylib_shell.dat};
          \addplot[black] table[x=size ,y=time, skip first n=0] {dat/benchmark_adjlib_intro.dat};
          \addplot[pink] table[x=size ,y=time, skip first n=0] {dat/benchmark_adjlib_insertion.dat};
          \addplot[orange] table[x=size ,y=time, skip first n=0] {dat/benchmark_adjlib_heap.dat};
          \legend{
            STL sort,  
            Bubble,
            Shell,
            Introsort,
            Insertion,
            Heap
          }
        \end{axis}
      \end{tikzpicture} 
      \caption{Graph showing benchmark results. The y-axis represents execution time in milliseconds, the x-axis
      represents the number of elements}
      \label{fig:bench_sort}
    \end{figure}

    \begin{figure}[btp]
      \begin{tikzpicture}
        \begin{axis}[
          xmajorgrids=false,ymajorgrids=false,
          legend pos=north west,
          width=1.0\textwidth, height=0.25\textheight,
%          reverse legend
          ]

          \addplot[green] table[x=size ,y=time, skip first n=0] {dat/benchmark_stl_sort.dat};
          \addplot[black] table[x=size ,y=time, skip first n=0] {dat/benchmark_adjlib_intro.dat};
          \addplot[orange] table[x=size ,y=time, skip first n=0] {dat/benchmark_adjlib_heap.dat};
          \legend{
            STL sort,
            Introsort,
            Heap
          }
        \end{axis}
      \end{tikzpicture}
      \caption{Graph comparing only introsort, heap, and STL sort}
      \label{fig:intro_vs_stl}
    \end{figure}

  \section{Concluding remarks}
  By comparing the benchmarks of the different sorting algorithms, it is clear that the implementation of introspective
  sorting using insertion sort and heap sort performs better than shell, bubble, insertion sort and heap sort, and is close
  to the execution time of STL.
  However, I suspect that the low time is the result of using STL's heap implementation. If I had implemented a custom heap
  sort function, I believe the execution time would be higher. Still, the implementation did perform better than standalone
  heap sort, which means the insertion sort algorithm did its job on the partially sorted vector.
\newline The benchmarks were only run once, and due the random nature of the integers being generated, one might end up with
a dataset which performs better in certain algorithms. To get the most accurate result it could be wise to run the benchmarks
multiple times, and computing the average execution time for each algorith.
\newline Further development could include implementing a fully custom heap sort function, and not just using the built-in
STL heap.



  % Include the bibliography
  \bibliographystyle{plain}
  \bibliography{bibliography}

\end{document}
