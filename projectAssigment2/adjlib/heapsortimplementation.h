#ifndef HEAPSORTIMPLEMENTATION
#define HEAPSORTIMPLEMENTATION

#include <vector>
#include <algorithm>
#include <cmath>
#include <typeinfo>

namespace adjlib {
    template <class T>
    class CustomHeap {
    private:
        std::vector<T> _vector;

    public:
        void add_to_heap(T item);
        T remove_from_heap();
    };

    template <class RandomAccessIterator>
    void heap_sort(RandomAccessIterator first, RandomAccessIterator last) {

        // The lazy way
        std::make_heap(first, last);
        std::sort_heap(first, last);

    }

    // In order to implement a custom version of heapsort, I found it necessary to make my own
    // heap implementation. Or, I could just use std::make_heap(begin, end) and std::sort_heap(begin, end)
    /*
    template <class T>
    void add_to_heap(T item) {
        _vector.push_back(item);
        int currentIndex = _vector.size() - 1;

        while (currentIndex > 0) {
            int parentIndex = (currentIndex - 1) / 2;

            if (_vector.at(currentIndex) > _vector.at(parentIndex)) {
                T temp = _vector.at(currentIndex);
                _vector.assign(currentIndex, _vector.at(parentIndex));
                _vector.assign(parentIndex, temp);
            }
            else
                break;

            currentIndex = parentIndex;
        }
    }

    template <class T>
    T remove_from_heap() {
        if (_vector.size() == 0)
            return nullptr;

        T removedObject = _vector.at(0);
        _vector.assign(0, _vector.at(_vector.size() - 1));
        _vector.pop_back();

        int currentIndex = 0;
        while (currentIndex < _vector.size()) {
            int leftChildIndex = 2 * currentIndex + 1;
            int rightChildIndex = 2 * currentIndex + 2;

            // Find the maximum between two children
            if (leftChildIndex >= _vector.size()) break;
            int maxIndex = leftChildIndex;
            if (rightChildIndex < _vector.size()) {
                if (_vector.at(maxIndex) < _vector.at(rightChildIndex))
                    maxIndex = rightChildIndex;
            }

            if (_vector.at(currentIndex) < _vector.at(maxIndex)) {
                T temp = _vector.at(maxIndex);
                _vector.assign(maxIndex, _vector.at(currentIndex));
                _vector.assign(currentIndex, temp);
                currentIndex = maxIndex;
            }
            else
                break;
        }

        return removedObject;
    }
    */
}

#endif // HEAPSORTIMPLEMENTATION

