#ifndef INSERTIONSORTIMPLEMENTATION
#define INSERTIONSORTIMPLEMENTATION

#include <list>
#include <algorithm>
#include <cmath>
#include <typeinfo>

namespace adjlib {

    // Insertion sort algorithm taken from http://www.sorting-algorithms.com/insertion-sort 11/09-15
    // and http://www.codecodex.com/wiki/Insertion_sort 11/09-15
    template <class RandomAccessIterator>
    void insertion_sort(RandomAccessIterator first, RandomAccessIterator last) {

        // If empty, do nothing
        if (first == last)
            return;

        // Assume the first element is the smallest
        RandomAccessIterator smallest = first;
        for (RandomAccessIterator next = first + 1; next < last; next++) {
            if(*next < *smallest)
                // Next element is smaller
                smallest = next;
        }

        std::iter_swap(first, smallest);
        while (++first < last){
            // Put the elements in the proper place
            for (RandomAccessIterator current = first; *current < *(current - 1); current--) {
                std::iter_swap((current - 1), current);
            }
        }
    }
}

#endif // INSERTIONSORTIMPLEMENTATION

