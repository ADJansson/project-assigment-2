
#include <gtest/gtest.h>

#include "../introsortimplementation.h"

#include <vector>



// Helper function
template <typename T>
::testing::AssertionResult VectorsMatch( const std::vector<T>& expected, const std::vector<T>& actual ){

    // Test size
    if( expected.size() != actual.size() )
        return ::testing::AssertionFailure() << "size mismatch: |vector| (" << actual.size() << ") != |expected| (" << expected.size() << ")";

    // Test per element content
    for( typename std::vector<T>::size_type i {0}; i < expected.size(); ++i )
        if( expected.at(i) != actual.at(i) )
            return ::testing::AssertionFailure() << "vector[" << i
                                                 << "] (" << actual.at(i) << ") != expected[" << i
                                                 << "] (" << expected.at(i) << ")";

    return ::testing::AssertionSuccess();
}


// Vector conatiner initialization tests
TEST(Algorithm_IntroSort, BasicSort) {

    // Arrange
    std::initializer_list<int> gold{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    std::initializer_list<int> data{8, 10, 7, 1, 4, 3, 9, 2, 5, 6};

    std::vector<int> vec_gold{gold};
    std::vector<int> vec{data};

    // Act
    adjlib::introsort(vec.begin(), vec.end());

    // Assert
    EXPECT_TRUE( VectorsMatch(vec_gold, vec) );

}


// Vector conatiner initialization tests
TEST(Algorithm_IntroSort,BasicSortNegativeValues) {

    // Arrange
    std::initializer_list<int> gold{-5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    std::initializer_list<int> data{-4, 8, 10, -1, 7, 1, 4, 3, -2, -3, 9, 2, 5, -5, 0, 6};

    std::vector<int> vec_gold{gold};
    std::vector<int> vec{data};

    // Act
    adjlib::introsort(vec.begin(), vec.end());

    // Assert
    EXPECT_TRUE( VectorsMatch(vec_gold, vec) );

}
