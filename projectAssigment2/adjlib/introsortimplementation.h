#ifndef INTROSORTIMPLEMENTATION
#define INTROSORTIMPLEMENTATION

#include <list>
#include <algorithm>
#include <cmath>
#include <typeinfo>
#include "insertionsortimplementation.h"
#include "heapsortimplementation.h"

namespace adjlib {

// Definitions
template <class RandomAccessIterator, class difference_type = std::less<typename RandomAccessIterator::difference_type>>
void introsort_loop(RandomAccessIterator first, RandomAccessIterator last, difference_type depth_limit);

template <class T>
T median_of_3(const T &first, const T &middle, const T &last);

int _stl_threshold = 7;

// Implementation
template <class RandomAccessIterator>
void introsort( RandomAccessIterator first, RandomAccessIterator last) {

    introsort_loop(first, last, (2 * std::floor(std::log(last - first))));
    insertion_sort(first, last);
}

template <class RandomAccessIterator, class difference_type = std::less<typename RandomAccessIterator::difference_type>>
void introsort_loop(RandomAccessIterator first,
                    RandomAccessIterator last, difference_type depth_limit) {
    using value_type = typename RandomAccessIterator::value_type;

    while (last - first > _stl_threshold) {
        if (depth_limit == 0) {
            heap_sort(first, last);
            return;
        }
        --depth_limit;

        auto pivot = median_of_3(*first, *(first + (last - first) / 2), *(last - 1));

        // http://en.cppreference.com/w/cpp/algorithm/partition 28/08-15
        RandomAccessIterator cut = std::partition(first, last,
                                                  [pivot](const value_type& em){ return em < pivot; });

        introsort_loop(cut, last, depth_limit);

        last = cut;

    }
}

template <class T>
T median_of_3(const T &first, const T &middle, const T &last) {
    // Quick and dirty way of finding the middle element
    std::vector<T> tempList = {first, middle, last};
    insertion_sort(tempList.begin(), tempList.end());
    return tempList[1];
}

}

#endif // INTROSORTIMPLEMENTATION

