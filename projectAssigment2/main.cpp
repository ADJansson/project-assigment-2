#include <iostream>
#include <vector>
#include "adjlib/introsortimplementation.h"

using namespace std;

int main()
{
    std::cout << "yolo\n";
    std::vector<int> testVector = {8, 10, 7, 1, 4, 3, 9, 2, 5, 6};

    adjlib::introsort(testVector.begin(), testVector.end());

    for (auto i: testVector) {
        std::cout << "testVector: " << i << "\n";
    }

    return 0;
}

