// mylib
#include "../../mylibrary/benchmark.h"
#include "../../mylibrary/shellsort.h"
#include "../../adjlib/introsortimplementation.h"
#include "../../adjlib/insertionsortimplementation.h"
#include "../../adjlib/heapsortimplementation.h"

// stl
#include <chrono>
#include <vector>
#include <stdexcept>
#include <algorithm>

///
///
///   This program benchmarks run times when sorting
///
///

template <class Container>
size_t
time_std_sort( Container C ) {

  using namespace mylib;

  auto start = std::chrono::high_resolution_clock::now();
  std::sort( C.begin(), C.end() );
  auto end = std::chrono::high_resolution_clock::now();

  return std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count();
}

template <class Container>
size_t
time_mylib_bubble( Container C ) {

  using namespace mylib;

  auto start = std::chrono::high_resolution_clock::now();
  mylib::bubbleSort( C.begin(), C.end() );
  auto end = std::chrono::high_resolution_clock::now();

  return std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count();
}

template <class Container>
size_t
time_mylib_shell( Container C ) {

  using namespace mylib;

  auto start = std::chrono::high_resolution_clock::now();
  mylib::shellSort( C.begin(), C.end() );
  auto end = std::chrono::high_resolution_clock::now();

  return std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count();
}

template <class Container>
size_t
time_adjlib_intro( Container C ) {

  using namespace adjlib;

  auto start = std::chrono::high_resolution_clock::now();
  adjlib::introsort( C.begin(), C.end() );
  auto end = std::chrono::high_resolution_clock::now();

  return std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count();
}

template <class Container>
size_t
time_adjlib_insertion( Container C ) {

  using namespace adjlib;

  auto start = std::chrono::high_resolution_clock::now();
  adjlib::insertion_sort( C.begin(), C.end() );
  auto end = std::chrono::high_resolution_clock::now();

  return std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count();
}

template <class Container>
size_t
time_adjlib_heap( Container C ) {

  using namespace adjlib;

  auto start = std::chrono::high_resolution_clock::now();
  adjlib::heap_sort( C.begin(), C.end() );
  auto end = std::chrono::high_resolution_clock::now();

  return std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count();
}


int main(int /*argc*/, char** /*argv*/) try {

  namespace bm = mylib::benchmark;

  std::vector<size_t> fill_sizes {
    20
    ,100
    ,1000
    ,10000
    ,20000
    ,30000
    ,40000
    ,50000
    ,100000
  };


  std::cout << "Benchmarking sorting!" << std::endl;

  std::vector<bm::TimingData> timing_data_std_sort;
  std::vector<bm::TimingData> timing_data_mylib_bubble;
  std::vector<bm::TimingData> timing_data_mylib_shell;
  std::vector<bm::TimingData> timing_data_adjlib_intro;
  std::vector<bm::TimingData> timing_data_adjlib_insertion;
  std::vector<bm::TimingData> timing_data_adjlib_heap;
  for( const auto& fill_size : fill_sizes ) {

    std::cout << "  Generating data set N = " << fill_size << std::endl;
    auto C = bm::genDataTestSet<int>(fill_size);

    auto C_std = C;
    auto C_bubble = C;
    auto C_shell = C;
    auto C_intro = C;
    auto C_insertion = C;
    auto C_heap = C;

    // Time std::sort
    std::cout << "    Timing std::sort..." << std::endl;
    timing_data_std_sort.push_back(
      bm::TimingData(fill_size,
        time_std_sort(C_std)
      )
    );

    // Time mylib::bubbleSort
    std::cout << "    Timing mylib::bubbleSort..." << std::endl;
    timing_data_mylib_bubble.push_back(
      bm::TimingData(fill_size,
        time_mylib_bubble(C_bubble)
      )
    );

    // Time mylib::shellSort
    std::cout << "    Timing mylib::shellSort..." << std::endl;
    timing_data_mylib_shell.push_back(
      bm::TimingData(fill_size,
        time_mylib_shell(C_shell)
      )
    );

      // Time adjlib::introsort
      std::cout << "    Timing adjlib::introsort..." << std::endl;
      timing_data_adjlib_intro.push_back(
        bm::TimingData(fill_size,
          time_adjlib_intro(C_intro)
        )
      );

      // Time adjlib::insertionsort
      std::cout << "    Timing adjlib::insertion_sort..." << std::endl;
      timing_data_adjlib_insertion.push_back(
        bm::TimingData(fill_size,
          time_adjlib_insertion(C_insertion)
        )
      );

      // Time adjlib::heap_sort
      std::cout << "    Timing adjlib::heap_sort..." << std::endl;
      timing_data_adjlib_heap.push_back(
        bm::TimingData(fill_size,
          time_adjlib_heap(C_heap)
        )
      );

  }


  // Write to file
  bm::writeToDat(timing_data_std_sort,     "stl_sort");
  bm::writeToDat(timing_data_mylib_bubble, "mylib_bubble");
  bm::writeToDat(timing_data_mylib_shell,  "mylib_shell");
  bm::writeToDat(timing_data_adjlib_intro,  "adjlib_intro");
  bm::writeToDat(timing_data_adjlib_insertion,  "adjlib_insertion");
  bm::writeToDat(timing_data_adjlib_heap,  "adjlib_heap");

  return 0;
}
catch (std::ifstream::failure e) {
  std::cerr << "Exception opening/reading/closing file: " << e.what();
}
catch(const std::exception& e){
  std::cerr << "An exception occurred: " << e.what() << std::endl;
}
catch(...) {
  std::cerr << "Unknown exception thrown!" << std::endl;
}
